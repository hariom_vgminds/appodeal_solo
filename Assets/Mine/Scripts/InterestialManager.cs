﻿using UnityEngine;
using System.Collections;

public class InterestialManager : MonoBehaviour 
{
	private IInterestialAds[] interestialAds;
	private IRewardVideoAds[] rewardAds;

	private IAdCompleteBehaviour onComplete;

	public static InterestialManager Instance;

	private void Awake()
	{
		Instance = this;
		interestialAds = GetComponentsInChildren<IInterestialAds> ();
		rewardAds = GetComponentsInChildren<IRewardVideoAds> ();
	}

	private void OnSceneChange()
	{
		onComplete = null;
	}

	private bool IsInterestialReady()
	{
//		if (SoomlaHandler.Instance.IfAnyItemPurchased())
//		{
//			return false;
//		}
		foreach (IInterestialAds interestialAd in interestialAds) 
		{
			if (interestialAd.IsInterestialReady ()) 
			{
				return true;
			}
		}
		return false;
	}

	private void ShowInterestial (IAdCompleteBehaviour onComplete = null)
	{
//		if (SoomlaHandler.Instance.IfAnyItemPurchased())
//		{
//			return;
//		}
		this.onComplete = onComplete;
		foreach (IInterestialAds interestialAd in interestialAds) 
		{
			if (interestialAd.IsInterestialReady ()) 
			{
				interestialAd.ShowInterestialAd ();
				return;
			}
		}
	}

	private bool IsRewardVideoReady()
	{
		foreach (IRewardVideoAds rewardAd in rewardAds) 
		{
			if (rewardAd.IsRewardVideoReady ()) 
			{
				return true;
			}
		}
		return false;
	}

	private void ShowRewardVideo (IAdCompleteBehaviour onComplete = null)
	{
//		if (SoomlaHandler.Instance.IfAnyItemPurchased())
//		{
//			return;
//		}
		this.onComplete = onComplete;
		foreach (IRewardVideoAds rewardAd in rewardAds) 
		{
			if (rewardAd.IsRewardVideoReady ()) 
			{
//				AnalyticsScript.Instance.SendEvent ("Reward Videos", "IsShown", rewardAd.ToString());
				rewardAd.ShowRewardVideo ();
				return;
			}
		}
//		AnalyticsScript.Instance.SendEvent ("Reward Videos", "IsShown", "false");
	}

	public void OnComplete()
	{
		if (onComplete != null)
		{
			onComplete.OnAdComplete ();
			onComplete = null;
//			if(LocalStorage.GetDaysSinceInstalled() == 0)
//				MyOneSignal.Instance.SyncTags (AppConstants.OS_REWARD_VIDEO_FINISHED_ON_FIRST_DAY, true);
		}
	}

	public void ShowAd(IAdCompleteBehaviour onComplete)
	{
		if (IsRewardVideoReady ())
		{
			ShowRewardVideo (onComplete);
			return;
		}	
		ShowInterestial (onComplete);
	}

	public void ShowAd(IAdCompleteBehaviour onComplete, bool preferRewardVideo)
	{
		if( IsAdReady ())
		{
			if (preferRewardVideo && IsRewardVideoReady ())
			{
				ShowRewardVideo (onComplete);
				return;
			}
			ShowInterestial (onComplete);
		}
		else
		{
			onComplete.OnAdComplete();
		}
	}

	public bool IsAdReady ()
	{
		return IsRewardVideoReady () || IsInterestialReady ();
	}

	void OnGUI()
	{
		GUILayout.BeginArea(new Rect(200,300,400,400));
		GUILayout.Toggle(IsAdReady (), "Ad Ready");
		if(GUILayout.Button("ShowAd"))
		{
			ShowAd(null);
		}
		GUILayout.EndArea();
	}
}

public interface IInterestialAds
{
	void RequestInterestialAd ();
	void ShowInterestialAd ();
	bool IsInterestialReady ();
}

public interface IRewardVideoAds
{
	void RequestRewardVideo ();
	void ShowRewardVideo ();
	bool IsRewardVideoReady ();
}

public interface IAdCompleteBehaviour
{
	void OnAdComplete();
}