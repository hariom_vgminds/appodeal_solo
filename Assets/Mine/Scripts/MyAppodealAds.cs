﻿using UnityEngine;
using System.Collections;
using AppodealAds.Unity.Api;
using AppodealAds.Unity.Common;

public class MyAppodealAds : MonoBehaviour, IInterestialAds, IBannerAds, IRewardVideoAds, IInterstitialAdListener, IBannerAdListener, IRewardedVideoAdListener, IPermissionGrantedListener
{
	#if UNITY_EDITOR && !UNITY_ANDROID && !UNITY_IPHONE
	string appKey = AppConstants.APPODEALID;
	#elif UNITY_ANDROID
	string appKey = AppConstants.APPODEALID;
	#elif UNITY_IPHONE
	string appKey = AppConstants.APPODEALID;
	#else
	string appKey = AppConstants.APPODEALID;
	#endif

	private string[] disbaledAppodealAdapters = new string[]
	  {"amazon_ads", "unity_ads", "mailru", "yandex", 
	  "startapp", "avocarrot", "flurry", "cheetah", 
	"inner-active", "revmob" ,"ogury" , "chartboost"};

	public void Awake()
	{
		Appodeal.requestAndroidMPermissions(this);
	}

	public void Start() 
	{
		DisableAppodealAdapters ();
		#if DEV_MODE
		Appodeal.setLogging(true);
		Appodeal.setTesting(true);
		#else
		Appodeal.setLogging(false);
		Appodeal.setTesting(false);
		#endif
		Appodeal.setBannerCallbacks (this);
		Appodeal.setInterstitialCallbacks (this);
		Appodeal.setRewardedVideoCallbacks(this);
		Appodeal.disableWriteExternalStoragePermissionCheck();
		Appodeal.initialize (appKey, Appodeal.INTERSTITIAL | Appodeal.BANNER | Appodeal.REWARDED_VIDEO);
	}

	public void ShowInterestialAd () 
	{
		Appodeal.show (Appodeal.INTERSTITIAL, "interstitial_button_click");
	}

	void DisableAppodealAdapters()
	{
		foreach(string str in disbaledAppodealAdapters)
		{
			Appodeal.disableNetwork(str);
		}
	}

	public void RequestRewardVideo ()
	{

	}
	public void ShowRewardVideo ()
	{
		Appodeal.show (Appodeal.REWARDED_VIDEO, "rewarded_video_button_click");
	}

	public bool IsRewardVideoReady ()
	{
		return Appodeal.isLoaded (Appodeal.REWARDED_VIDEO); 
	}

	public void RequestInterestialAd ()
	{

	}

	public bool IsInterestialReady ()
	{
		return Appodeal.isLoaded (Appodeal.INTERSTITIAL); 
	}

	public void showRewardedVideo() 
	{
		Appodeal.show (Appodeal.REWARDED_VIDEO, "rewarded_video_button_click");
	}

	public void ShowBannerAd(BannerPos pos) 
	{
		DestroyBannerAd ();
		if(pos == BannerPos.BOTTOM)
		Appodeal.show (Appodeal.BANNER_BOTTOM, "banner_button_click");
		else
		Appodeal.show (Appodeal.BANNER_TOP, "banner_button_click");
	}

	public void showInterstitialOrVideo() 
	{
		Appodeal.show (Appodeal.INTERSTITIAL);
	}

	public bool IsBannerReady ()
	{
		return Appodeal.isLoaded (Appodeal.BANNER); 
	}

	public void DestroyBannerAd() 
	{
		Appodeal.hide (Appodeal.BANNER_BOTTOM);
		Appodeal.hide (Appodeal.BANNER_TOP);
	}

	#region Banner callback handlers

	public void onBannerLoaded() { print("Banner loaded");}
	public void onBannerFailedToLoad() { print("Banner failed");}
	public void onBannerShown() { print("Banner opened"); }
	public void onBannerClicked() { print("banner clicked"); }

	#endregion

	#region Interstitial callback handlers

	public void onInterstitialLoaded() { print("Interstitial loaded"); }
	public void onInterstitialFailedToLoad() { print("Interstitial failed"); }
	public void onInterstitialShown() { print("Interstitial opened"); }
	public void onInterstitialClicked() { print("Interstitial clicked"); }
	public void onInterstitialClosed() { print("Interstitial closed"); Invoke("InterestialOrRewardClosed",0.1f); }

	#endregion

	public void InterestialOrRewardClosed()
	{
		InterestialManager.Instance.OnComplete ();
	}

	#region Non Skippable Video callback handlers

	public void onNonSkippableVideoLoaded() { print("NonSkippable Video loaded"); }
	public void onNonSkippableVideoFailedToLoad() { print("NonSkippable Video failed"); }
	public void onNonSkippableVideoShown() { print("NonSkippable Video opened"); }
	public void onNonSkippableVideoClosed() { print("NonSkippable Video closed"); }
	public void onNonSkippableVideoFinished() { print("NonSkippable Video finished"); }

	#endregion

	#region Rewarded Video callback handlers

	public void onRewardedVideoLoaded() { Debug.Log("Rewarded Video loaded"); }
	public void onRewardedVideoFailedToLoad() { Debug.Log("Rewarded Video failed"); }
	public void onRewardedVideoShown() { Debug.Log("Rewarded Video opened"); }
	public void onRewardedVideoClosed() { Debug.Log("Rewarded Video closed");}
	public void onRewardedVideoFinished(int amount, string name) {Invoke ("InterestialOrRewardClosed", 0.1f); }

	#endregion

	#region Permission Grant callback handlers

	public void writeExternalStorageResponse(int result) { 
	if(result == 0) {
	print("WRITE_EXTERNAL_STORAGE permission granted"); 
	} else {
	print("WRITE_EXTERNAL_STORAGE permission grant refused"); 
	}
	}
	public void accessCoarseLocationResponse(int result) { 
	if(result == 0) {
	print("ACCESS_COARSE_LOCATION permission granted"); 
	} else {
	print("ACCESS_COARSE_LOCATION permission grant refused"); 
	}
	}

	#endregion

	}
