﻿using UnityEngine;
using System.Collections;

public class BannerAdsManager : MonoBehaviour 
{
	public IBannerAds[] bannerAds;

	public static BannerAdsManager Instance;

	public RectTransform[] Panels;
	private float bannerMargin = -150f;

	void Awake()
	{
		Instance = this;
		bannerAds = GetComponentsInChildren<IBannerAds> ();
	}

	public void ShowAd(BannerPos pos)
	{
//		if (SoomlaHandler.Instance.IfAnyItemPurchased())
//		{
//			return;
//		}

		foreach (IBannerAds bannerAd in bannerAds) 
		{
			if (bannerAd.IsBannerReady ())
			{
				bannerAd.ShowBannerAd (pos);
				foreach(RectTransform panel in Panels)
					panel.offsetMax = new Vector2(0, bannerMargin);
				return;
			}
		}
	}

	public void ShowTopAd()
	{
		ShowAd (BannerPos.TOP);
	}

	public void ShowBottomAd()
	{
		ShowAd (BannerPos.BOTTOM);
	}
		
	public void DestroyAd()
	{
		foreach (IBannerAds bannerAd in bannerAds) 
		{
			bannerAd.DestroyBannerAd ();
		}
		foreach (RectTransform panel in Panels)
			panel.offsetMax = Vector2.zero;
	}

	void OnGUI()
	{
		GUILayout.BeginArea(new Rect(600,300,400,400));
		if(GUILayout.Button("ShowAd"))
		{
			ShowAd(BannerPos.TOP);
		}
		if(GUILayout.Button("DestroyAd"))
		{
			DestroyAd();
		}
		GUILayout.EndArea();
	}
}

public enum BannerPos
{
	TOP,
	BOTTOM
}

public interface IBannerAds
{
	bool IsBannerReady ();
	void ShowBannerAd (BannerPos pos);
	void DestroyBannerAd ();
}